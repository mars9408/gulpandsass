var gulp = require('gulp'),
    compass = require('gulp-compass'),
    // 處理例外與錯誤
    plumber = require('gulp-plumber'),
    // 監看gulp執行狀況
    // watch = require('gulp-watch'),
    // check js syntax
    jshint = require('gulp-jshint'),
    concat = require('gulp-concat'),
    // rename
    rename = require('gulp-rename'),
    // ugilfy
    uglify = require('gulp-uglify'),
    wiredep = require('wiredep').stream;

// v4監看gulp執行狀況
const { watch } = require('gulp');

// 套件主要檔案移植到建置目錄
var mainBowerFiles = require('main-bower-files');

// 定義路徑
var path = {
  'source': './source/',
  'build': './build/',
  'bower' : './bower_components/',
  'css'   : './source/stylesheets/',
  'img'   : './source/images/',
  'js'   : './source/js/',
  'public': './public/'
}

// watch at beginning
gulp.task('watch', function () {
  // assgin to all file not only for javascript
  // gulp.watch(path.js + '**', ['scripts']);
  gulp.watch(path.build + '*.js', ['scripts']);
});


// Lint JS
gulp.task('jslint', function () {
  return gulp.src('src/*.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'));
});


// exec compass
gulp.task('compass', function () {
  gulp.src(['./source/scss/'])
    .pipe(plumber())
    .pipe(compass({
      // 注意：這裡必需額外加入config.rb
      config_file: './config.rb'
    }));
  // gulp v4
  return new Promise(function (resolve, reject) {
    console.log("compass done!");
    resolve();
  });
});


// bower packages move to ./build/bower/
gulp.task('bowerotbuild', function () {
  gulp.src(mainBowerFiles())
      .pipe(gulp.dest(path.build + 'bower/'));
  return new Promise(function (resolve, reject) {
    console.log("bower files genterared!");
    resolve();
  });
});


// move self-develop js to build/
gulp.task('jsmove', function () {
  gulp.src(path.source+'js/*.js')
      .pipe(rename({
        // dirname: "main/text/ciao",
        // basename: "aloha",
        prefix: "dev-",
        // suffix: "-hola",
        extname: ".js"
      }))
      .pipe(gulp.dest(path.build));
  // gulp.src(path.source + 'js/*.js')
  //     .pipe(gulp.dest(path.build));

  return new Promise(function (resolve, reject) {
    console.log("javascript move done!");
    resolve();
  });
});


// wiredep auto paste bower packages
gulp.task('wiredep', function () {
  gulp.src(path.source + '/index.html')
    .pipe(wiredep({
      optional: 'configuration',
      goes: 'here'
    }))
    .pipe(gulp.dest(path.public));
  return new Promise(function (resolve, reject) {
    console.log("Wiredep inserted!");
    resolve();
  });
});


// Concat & Minify & Mangle JS
gulp.task('minify', function () {
  gulp.src(path.build + '**/*.js')
      // Concat
      .pipe(concat('all.js'))
      .pipe(gulp.dest(path.build))
      // Minify
      .pipe(uglify({
        // Mangle
        mangle: true
      }))
      .pipe(rename('all.min.js'))
      .pipe(gulp.dest(path.public + 'js'));

  return new Promise(function (resolve, reject) {
    console.log("Javascripts concat with uglify and mangle done!");
    resolve();
  });

});


gulp.task('default', gulp.series('jslint', 'jsmove',
  'bowerotbuild', 'compass', 'minify'));