(function ($) {

  $.fn.extend({
    model: function () {
      var numberType = "data-prop-number";

      var elements = this.find("[data-prop],[" + numberType + "]");

      if (elements.length === 0) return undefined;

      var obj = {};

      $.each(elements,
        function (index, element) {
          if (element.dataset.propNumber) {
            obj[element.dataset.propNumber] = Number($(element).val());
          } else {
            obj[element.dataset.prop] = $(element).val();
          }
        });

      return obj;
    }
  });

})(jQuery);