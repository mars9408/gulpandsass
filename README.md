# gulpandsass

以客製化bootstrap v4.4.1為主軸，使用gulp進行自動化打包、壓縮與js混淆機制範本建立與流程說明。
---

首先先確認可執行yarn(或npm)、bower，假設沒有yarn(npm)請參照官網根據你的環境安裝，bower指令也是。

以下以前述為前提並於本專案目錄下執行：

### 透過bower安裝bootstrap/jquery
```bash
$ bower install bootstrap jquery
bower bootstrap#*           not-cached https://github.com/twbs/bootstrap.git#*
bower bootstrap#*              resolve https://github.com/twbs/bootstrap.git#*
bower jquery#*                  cached https://github.com/jquery/jquery-dist.git#3.5.0
bower jquery#*                validate 3.5.0 against https://github.com/jquery/jquery-dist.git#*
bower bootstrap#*             download https://github.com/twbs/bootstrap/archive/v4.4.1.tar.gz
bower bootstrap#*              extract archive.tar.gz
bower bootstrap#*             resolved https://github.com/twbs/bootstrap.git#4.4.1
bower jquery#^3.5.0            install jquery#3.5.0
bower bootstrap#^4.4.1         install bootstrap#4.4.1

jquery#3.5.0 bower_components/jquery

bootstrap#4.4.1 bower_components/bootstrap
```
完成後產生 bower_components 目錄

### 透過yarn安裝本地端gulp
```bash
$yarn add gulp
yarn add v1.22.4
info No lockfile found.
[1/4] 🔍  Resolving packages...
warning gulp > glob-watcher > chokidar@2.1.8: Chokidar 2 will break on node v14+. Upgrade to chokidar 3 with 15x less dependencies.
warning gulp > glob-watcher > chokidar > fsevents@1.2.12: fsevents 1 will break on node v14+ and could be using insecure binaries. Upgrade to fsevents 2.
warning gulp > glob-watcher > anymatch > micromatch > snapdragon > source-map-resolve > urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
warning gulp > glob-watcher > anymatch > micromatch > snapdragon > source-map-resolve > resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
[2/4] 🚚  Fetching packages...
[3/4] 🔗  Linking dependencies...
[4/4] 🔨  Building fresh packages...
success Saved lockfile.
success Saved 247 new dependencies.
info Direct dependencies
└─ gulp@4.0.2
info All dependencies
├─ abbrev@1.1.1
├─ ansi-colors@1.1.0
# ...大量套件不表列

# 確認已完成安裝
$ gulp -v
# gulp-cli version
CLI version: 2.2.0
# 專案目錄安裝gulp版本 v4
Local version: 4.0.2
```
完成後產生`node_modules目錄`、`package.json`、`yarn.lock`檔案，yarn.lock檔案可以作為前端團隊協同開發確認使用一致套件與版本用途。
> 將yarn.lock分享給他人，不用配置package.json直接下`yarn install`即會依照檔案內套件與版本建置node_modules。

## 利用sass製作以bootstrap為基礎建立自己的風格
首先因為後續`gulp`會用到`gulp-compass`，需要確認環境有`ruby`，參考[Gulp RUN ruby-compass](https://wcc723.github.io/gulp/2014/11/07/gulp-on-diff-os/)的 _安裝Compass_ 片段說明進行確認。

### 建立config.rb檔
```ruby
# 匯入 Bootstrap v4.4.1 來自bower安裝的路徑下
additional_import_paths = "./bower_components/bootstrap/scss/"
# 輸出路徑
css_path = "public/styles"
sass_path = "source/scss"
images_dir = "source/images"
output_style = :nest
relative_assets = true
line_comments = false
sass_options = {:debug_info=>false}
sourcemap = true
```
> 在config.rb內加入`sourcemap = true`設定，對於css開發者來說，就可以省去很多時間找css code了（所以sourcemap只適用於開發環境）

### 配置sass基礎內容
根據前述`config.rb`內容配置，建立`source/scss`目錄，並於目錄內在建立`all.scss`檔案及`helpers`目錄旗下放置`_variables.scss`檔案

> 此部分的配置，我直接參考<https://github.com/Wcc723/gulp-compass-bootstrap-demo>作為範本進行符合bt v4.4.1可套用情況。
> 所以直接把相關修改註解於前述檔案內。

### 可執行gulp確認產出css結果

#### 安裝必要gulp套件
```bash
$yarn add gulp-compass
```

#### 安裝功能性gulp套件，處理過程監看與錯誤例外機制
```bash
$yarn add gulp-watch gulp-plumber
```

#### 目前gulpfile.js
```js
var gulp = require('gulp'),
    compass = require('gulp-compass'),
    // 處理例外與錯誤
    plumber = require('gulp-plumber'),
    // 監看gulp執行狀況
    watch = require('gulp-watch');

// 定義路徑
var path = {
  'source': './source/',
  'build': './build/',
  'bower' : './bower_components/',
  'css'   : './source/stylesheets/',
  'img'   : './source/images/',
  'public': './public/'
}

// exec compass
gulp.task('compass', function () {
  gulp.src(['./source/scss/'])
    .pipe(plumber())
    .pipe(compass({
      // 注意：這裡必需額外加入config.rb
      config_file: './config.rb'
    }));
  // gulp v4
  return new Promise(function (resolve, reject) {
    console.log("compass done!");
    resolve();
  });
});
```

#### 執行
```bash
$ gulp compass
[16:11:53] Using gulpfile /usr/local/var/www/gulpsass/gulpfile.js
[16:11:53] Starting 'compass'...
compass done!
[16:11:53] Finished 'compass' after 10 ms
directory public/styles

    write public/styles/all.css

    write public/styles/all.css.map
```
於專案目錄下產生public目錄放置styles/all.css與all.css.map檔，all.css檔即為改過的bootstrap.css完整內容。
> 若將前述source/`all.scss`檔案改名為`bootstrap.scss`那麼產出的就會是`bootstrap.css`
> sass/scss都透過sass進行編譯產出，只是scss語法更接近一般css寫法習慣
> 開頭以`_`命名檔案不會產出對應css

---

## 安裝js第三方套件
以dataTables與fullCalendar為例，分別參考[datatables｜Bower packages](https://datatables.net/download/bower)以及[fullcalendar on Bower - Libraries.io](https://libraries.io/bower/fullcalendar)的指令安裝

```bash
# 安裝datatables以及bootstap v4為基礎版型
$bower install --save datatables.net
$bower install --save datatables.net-bs4
# 安裝full calendar
$bower install fullcalendar
```

### 透過main-bower-files將套件主要載入移植到建置目錄
安裝後的套件檔案存在於個別根目錄下dist內，透過此檔案可集中複製於build目錄進行處理後，在存放於public/。


> 此時必須先建立bower.json定義描述檔，而在bower.json內針對bootstrap必須指定複寫路徑對於自動化指令才能正確找到應該進行搬移動作的檔案位置。

```json
{
  ...
  "overrides": {
    "bootstrap": {
      "main": [
        "dist/js/bootstrap.js",
        "dist/css/bootstrap.min.css",
        "less/bootstrap.less"
      ]
    }
  },
  ...
}
```

改寫gulpfile.js
```js
// 載入main-bower-files
var mainBowerFiles = require('main-bower-files');

// bower packages move to ./build/bower/
gulp.task('bowerotbuild', function () {
  gulp.src(mainBowerFiles())
      .pipe(gulp.dest(path.build + 'bower/'));
  return new Promise(function (resolve, reject) {
    console.log("bower files genterared!");
    resolve();
  });
});
```

> 狀況：此時完成後僅有bootstrap與jquery.js被移植到build目錄夾下！fullcalendar與dataTables則沒有？

## 透過wiredep將專案相依套件直接配置載入程式碼到指定位置
由於透過bower安裝套件可能會隨時間持續增減， 而每次調整後可以透過 `gulp-wiredep`機制來進行自動幫你改寫載入語法。
> 此一方式不侷限於針對index.html， 甚至php後端專案的config.php亦可套用！

```bash
$yarn add gulp-wiredep -d
```

改寫gulpfile.js
```js
var wiredep = require('wiredep').stream;

// wiredep auto paste bower packages
gulp.task('wiredep', function () {
  gulp.src(path.source + '/index.html')
    .pipe(wiredep({
      optional: 'configuration',
      goes: 'here'
    }))
    .pipe(gulp.dest(path.public));
  return new Promise(function (resolve, reject) {
    console.log("Wiredep inserted!");
    resolve();
  });
});
```

執行
```bash
$gulp wiredep
[17:00:43] Using gulpfile /usr/local/var/www/gulpsass/gulpfile.js
[17:00:43] Starting 'wiredep'...
Wiredep inserted!
[17:00:44] Finished 'wiredep' after 9.38 ms
```